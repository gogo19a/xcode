//
//  ViewController.swift
//  Magnet
//
//  Created by admin on 3/16/22.
//

import UIKit
import WebKit


class ViewController: UIViewController {

    let webview:WKWebView={
        let prefs=WKWebpagePreferences()
        prefs.allowsContentJavaScript=true
        let config=WKWebViewConfiguration()
        config.mediaTypesRequiringUserActionForPlayback = []
        config.defaultWebpagePreferences=prefs
        config.allowsInlineMediaPlayback=true

        let webview=WKWebView(
            frame: .zero,
            configuration: config
        )
        
        
        return webview
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setuplaysinline=1p after loading the view.
        view.addSubview(webview)
        guard let url=URL(string: "https://magnetcup.net?playsinline=1") else{
            return}
        
        webview.allowsBackForwardNavigationGestures=true
        webview.load(URLRequest(url: url))
        
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        webview.frame=view.bounds
        
    }
}

